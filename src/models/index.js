const mongoose = require('mongoose');
const User = require('./user.model');
const Sheet = require('./sheet.model');

mongoose.set('debug', true);
mongoose.connect('mongodb://test:asdf12@ds249311.mlab.com:49311/spread_sheets', { useNewUrlParser: true });

mongoose.Promise = Promise;
module.exports = { User, Sheet };
