const mongoose = require('mongoose');

const sheetSchema = new mongoose.Schema({
  name: {
    type: String,
    required: 'Sheet Name cannot be blank!',
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: 'Author cannot be blank!',
  },
  created: {
    type: Date,
    default: Date.now,
  },
  is_public: {
    type: Boolean,
    default: false,
  },
  data: {
    type: Object,
    default: [],
  },
});

const Sheet = mongoose.model('Sheet', sheetSchema);

module.exports = Sheet;
