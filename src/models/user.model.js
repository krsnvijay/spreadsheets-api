const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: 'Username cannot be blank!',
  },
  email: {
    type: String,
    required: 'Email cannot be blank!',
  },
  password: {
    type: String,
    required: 'Password cannot be blank!',
  },
  sharedSheets: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Sheet' }]
});

const User = mongoose.model('User', userSchema);

module.exports = User;
