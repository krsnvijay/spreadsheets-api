const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const jwt = require('koa-jwt');
const cors = require('@koa/cors');

const router = require('./routes');
const { errorMiddleware } = require('./utils/middlewares');

const app = new Koa();
const port = process.env.PORT || 3001;
const unprotectedPaths = [
  /^\/public/,
  /^\/api\/auth\/login/,
  /^\/api\/auth\/register/,
];

app.use(bodyParser());
app.use(cors());
app.use(errorMiddleware);
app.use(async (ctx, next) => {
  ctx.state.secret = 'pa$$word';
  await next();
});
app.use(jwt().unless({ path: unprotectedPaths }));
app.use(router.routes()).use(router.allowedMethods());
app.listen(port);