const Router = require('koa-router');
const apiRouter = require('./api.router');

const rootRouter = Router();

rootRouter
  .use(apiRouter.routes(), apiRouter.allowedMethods());

module.exports = rootRouter;
