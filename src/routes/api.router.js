const Router = require('koa-router');
const authRouter = require('./auth.router');
const userRouter = require('./user.router');
const sheetRouter = require('./sheet.router');

const apiRouter = Router({ prefix: '/api' });
apiRouter
  .get('/', (ctx) => {
    ctx.body = {
      message: 'API ROOT',
    };
  })
  .use(userRouter.routes(), userRouter.allowedMethods())
  .use(authRouter.routes(), authRouter.allowedMethods())
  .use(sheetRouter.routes(), sheetRouter.allowedMethods());

module.exports = apiRouter;
