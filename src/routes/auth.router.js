const Router = require('koa-router');
const { authController } = require('../controllers');

const authRouter = Router({ prefix: '/auth' });
authRouter
  .get('/info', authController.info)
  .post('/login', authController.login)
  .post('/register', authController.register);

module.exports = authRouter;
