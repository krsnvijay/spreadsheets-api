const Router = require('koa-router');
const { userController } = require('../controllers');

const usersRouter = new Router({ prefix: '/users' });
const userRouter = new Router({ prefix: '/:userId' });

// route:api/users
usersRouter
  .get('/', userController.index);

// route:api/users/:userId
userRouter
  .get('/', userController.show);

usersRouter
  .use(userRouter.routes(), userRouter.allowedMethods());


module.exports = usersRouter;
