const Router = require('koa-router');
const { sheetController } = require('../controllers');

const sheetsRouter = new Router({ prefix: '/sheets' });
const sheetRouter = new Router({ prefix: '/:sheetId' });

// route:api/sheets/
sheetsRouter
  .get('/', sheetController.index)
  .post('/', sheetController.create);

// route:api/sheets/:sheetId
sheetRouter
  .get('/', sheetController.show)
  .get('/download', sheetController.download)
  .post('/share', sheetController.share)
  .patch('/', sheetController.update)
  .delete('/', sheetController.delete);

sheetsRouter
  .use(sheetRouter.routes(), sheetRouter.allowedMethods());

module.exports = sheetsRouter;
