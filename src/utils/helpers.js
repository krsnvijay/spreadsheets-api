function list2CSV(list) {
  const replacer = (key, value) => (value === null ? '' : value);
  const csv = list.map(row => row.map(cell => JSON.stringify(cell, replacer)).join(','));
  return csv.join('\r\n').replace(/\"/g, '');
}

module.exports = {
  list2CSV,
};
