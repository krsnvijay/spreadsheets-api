const { User } = require('../models');

const userController = {
  async index(ctx) {
    try {
      // Get list of users
      const result = await User.find().select('-password -__v -sharedSheets');
      ctx.body = result;
    } catch (error) {
      ctx.throw(400, 'Error Fetching Users:');
    }
  },
  async show(ctx) {
    const { params } = ctx;
    if (!params.userId) ctx.throw(400, 'Invalid UserId');

    try {
      // Find and show user
      const user = await User.findById(params.userId).select('-password -__v -sharedSheets');
      ctx.body = user;
    } catch (error) {
      ctx.throw(error.status || 400, `Error Fetching User, ${error.message}`);
    }
  },
};
module.exports = userController;
