const { Sheet, User } = require('../models');
const { list2CSV } = require('../utils/helpers');
const mongoose = require('mongoose');

const sheetController = {
  async index(ctx) {
    try {
      const result = await Sheet.find({ author: ctx.state.user._id }).select('-data -__v');
      ctx.body = result;
    } catch (error) {
      ctx.throw(error.status || 400, 'Error Fetching Sheets');
    }
  },

  async show(ctx) {
    const { params } = ctx;
    if (!params.sheetId || !mongoose.Types.ObjectId.isValid(params.sheetId)) ctx.throw(400, 'Invalid sheetId');

    try {
      const sheet = await Sheet.findById(params.sheetId).select('-__v');
      const user = await User.findById(ctx.state.user._id);
      console.log();
      const isInArray = user.sharedSheets.some(s => s.equals(params.sheetId));
      if (sheet.author.equals(user._id) || isInArray || sheet.is_public) {
        ctx.body = sheet;
      } else ctx.throw(401, 'Unauthorized Access');
    } catch (error) {
      ctx.throw(error.status || 400, `Error Fetching Sheet, ${error.message}`);
    }
  },

  async create(ctx) {
    const { body } = ctx.request;
    body.author = ctx.state.user._id;
    try {
      const sheet = await Sheet.create(body);
      ctx.body = sheet;
      ctx.status = 201;
    } catch (error) {
      ctx.throw(400, 'Error Creating Sheet');
    }
  },

  async update(ctx) {
    const { params } = ctx;
    const { body } = ctx.request;
    if (!params.sheetId || !mongoose.Types.ObjectId.isValid(params.sheetId)) ctx.throw(400, 'Invalid sheetId');


    try {
      const sheet = await Sheet.findById(params.sheetId);
      if (sheet.author.equals(ctx.state.user._id)) {
        const sheet = await Sheet.findOneAndUpdate({ _id: params.sheetId }, body, {
          new: true,
        });
        ctx.body = sheet;
      } else ctx.throw(401, 'Unauthorized Access');
    } catch (error) {
      ctx.throw(error.status || 400, `Error Updating Sheet, ${error.message}`);
    }
  },
  async delete(ctx) {
    const { params } = ctx;
    if (!params.sheetId || !mongoose.Types.ObjectId.isValid(params.sheetId)) ctx.throw(400, 'Invalid sheetId');

    try {
      const sheet = await Sheet.findById(params.sheetId);
      if (sheet.author.equals(ctx.state.user._id)) {
        await Sheet.findOneAndRemove({ _id: params.sheetId });
        ctx.body = { message: 'Success' };
      } else ctx.throw(401, 'Unauthorized Access');
    } catch (error) {
      ctx.throw(error.status || 400, `Error Deleting Sheet, ${error.message}`);
    }
  },
  async share(ctx) {
    const { params } = ctx;
    const { body } = ctx.request;

    if (!params.sheetId
      || !body.userId
      || !mongoose.Types.ObjectId.isValid(body.userId)
      || !mongoose.Types.ObjectId.isValid(params.sheetId))
      ctx.throw(400, 'Invalid sheetId/userId');

    try {
      const user = await User.findById(body.userId);
      const sheet = await Sheet.findById(params.sheetId);
      if (user.sharedSheets.includes(sheet._id))
        ctx.throw(401, 'Sheet Already Shared');
      else {
        user.sharedSheets.push(sheet);
        user.save();
        ctx.body = { message: 'Shared With User Successfully' };
      }
    } catch (error) {
      ctx.throw(error.status || 400, `Error Sharing Sheet, ${error.message}`);
    }
  },

  async download(ctx) {
    const { params } = ctx;
    if (!params.sheetId || !mongoose.Types.ObjectId.isValid(params.sheetId)) ctx.throw(400, 'Invalid sheetId');

    try {
      const sheet = await Sheet.findById(params.sheetId);
      ctx.type = 'text/csv';
      if (sheet.author.equals(ctx.state.user._id) || sheet.is_public) {
        ctx.response.attachment(`${sheet.name}.csv`);
        ctx.body = list2CSV(sheet.data);
      } else {
        ctx.throw(401, 'Unauthorized Access');
      }
    } catch (error) {
      ctx.throw(error.status || 400, `Error Downloading Sheet, ${error.message}`);
    }
  },
};
module.exports = sheetController;
