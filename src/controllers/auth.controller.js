const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { User } = require('../models');

const authController = {
  async register(ctx) {
    const { body } = ctx.request;
    console.log(body)
    if (!body.username || !body.password) ctx.throw(400, 'Invalid Credentials');
    try {
      // create user
      const count = await User.count({ username: body.username });
      if (count > 0) { ctx.throw(400, 'Username Already Taken'); }
      const saltRounds = 10;
      const myPlaintextPassword = body.password;
      const hash = await bcrypt.hash(myPlaintextPassword, saltRounds);
      body.password = hash;
      let user = await User.create(body);
      const { _id, username, email } = user.toJSON();
      user = { _id, username, email };
      ctx.body = {
        ...user,
        token: jwt.sign(user, ctx.state.secret),
      };
      ctx.status = 201;
    } catch (error) {
      ctx.throw(400, `Error Creating User: ${error.message}`);
    }
  },
  async login(ctx) {
    const { body } = ctx.request;
    if (!body.username || !body.password) ctx.throw(400, 'Invalid Credentials');

    try {
      // Find and show user
      let user = await User.findOne({ username: body.username });
      if (user === null) ctx.throw(400, 'Invalid Credentials');
      const { _id, username, email, password: hash } = user.toJSON();
      user = { _id, username, email };
      const res = await bcrypt.compare(body.password, hash);
      if (res) {
        ctx.body = { token: jwt.sign(user, ctx.state.secret), ...user };
      } else {
        ctx.throw(400, 'Invalid Password');
      }
    } catch (error) {
      ctx.throw(error.status || 400, `Authentication Error, ${error.message}`);
    }
  },
  async info(ctx) {
    const { sharedSheets } = await User.findById(ctx.state.user._id).select('sharedSheets').populate({ path: 'sharedSheets', select: '-__v -data' });
    const user = { ...ctx.state.user, sharedSheets };
    ctx.body = user;

  },
};
module.exports = authController;
